<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use App\Models\Order;
use Livewire\Component;

class Dashboard extends Component
{
    public $total_orders;
    public $itemsCount;
    public $todayOrders;
    public $todayMoney;



    public function mount()
    {
        $this->total_orders = Order::count();
        $this->itemsCount = Order::sum('itemsCount');
        $this->todayOrders = Order::whereDate('created_at', Carbon::today())->count('itemsCount');
        $this->todayMoney = Order::whereDate('created_at', Carbon::today())->sum('price');
    }
    public function render()
    {
        return view('livewire.dashboard');
    }
}
