<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use Illuminate\Http\Request;
use App\Jobs\SendNotifyMailJob;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeUploadFileController extends Controller
{
    public function upload(Request $request)
    {

        $request->validate([
            'image' => 'required',
            'email' => 'required',
        ]);


        $path = $request->file('image')->store('images', 's3');
        Storage::disk('s3')->setVisibility($path, 'public');
        $path1 = Storage::disk('s3')->url($path);

        SendNotifyMailJob::dispatch($request->email, $path1);

        DB::table('guests')->insert([
            'email' => $request->email,
            'url' => $path1,
            'status' => true,
        ]);

        return  $path1;
    }

    public function index()
    {

        $guests = Guest::get();
        return view('guests')->with('guests', $guests);

    }
}
