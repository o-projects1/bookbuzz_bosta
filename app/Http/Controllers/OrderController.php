<?php

namespace App\Http\Controllers;

use App\Models\Order;
use GuzzleHttp\Client;
use Shopify\Clients\Rest;
use Illuminate\Support\Facades\Http;

class OrderController extends Controller
{
    public function index()
    {
        $response = Http::get('https://66ce4ef062e5aaa061113fdbcb1edc92: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@xn-btdbbgm6cc3bi8a1kffdahg6h.myshopify.com/admin/api/2022-01/orders.json?status=open');

        foreach ($response->collect()['orders'] as $order) {
            // return $order['order_number'];
            $row = Order::where('order_number_shopify', $order['order_number'])->first();
            if (!$row) {

                // dd($order['shipping_address']['city']);

                $customer_first_name = $order['shipping_address']['first_name'];
                $customer_address1 = $order['shipping_address']['address1'];
                $customer_phone = $order['shipping_address']['phone'];
                $customer_city = $order['shipping_address']['city'];
                $customer_zip = $order['shipping_address']['zip'];
                $customer_province = $order['shipping_address']['province'];
                $customer_country = $order['shipping_address']['country'];
                $customer_last_name = $order['shipping_address']['last_name'];
                $customer_address2 = $order['shipping_address']['address2'];
                $customer_name = $order['shipping_address']['name'];
                $customer_email = $order['customer']['email'];

                $price = $order['current_total_price_set']['shop_money']['amount']; //360
                $gateway = $order['gateway']; //accept_card
                $order_number = $order['order_number']; //3286

                $itemsCount = count($order['line_items']); //3 
                // $fulfillment_id = $order['fulfillments'][0]['id'];

                if ($itemsCount < 5) {
                    $size = 'SMALL';
                } elseif (in_array($itemsCount, range(5, 10))) {
                    $size = 'MEDIUM';
                } elseif ($itemsCount >= 10) {
                    $size = 'LARGE';
                }

                $order = Order::create([
                    'price' => $price,
                    'size' => $size,
                    'itemsCount' => $itemsCount,
                    'order_number_shopify' => $order_number,
                    'customer_first_name' => $customer_first_name,
                    'customer_last_name' => $customer_last_name,
                    'customer_address1' => $customer_address1,
                    'customer_address2' => $customer_address2,
                    'customer_phone' => $customer_phone,
                    'customer_email' => $customer_email,
                    'customer_province' => $customer_province,
                    'customer_city' => $customer_city
                ]);

                // try {
                //     $body = [
                //         "type" => 10,
                //         "specs" => [
                //             "size" => $size,
                //             "packageDetails" => [
                //                 "itemsCount" => $itemsCount,
                //                 "document" => "Document",
                //                 "description" => "Desc."
                //             ]
                //         ],
                //         "returnSpecs" => [
                //             "size" => "SMALL",
                //             "packageDetails" => [
                //                 "itemsCount" => 5,
                //                 "document" => "Document",
                //                 "description" => "Desc."
                //             ]
                //         ],
                //         "notes" => "Welcome Note From Fadl",
                //         "cod" => $price,
                //         "dropOffAddress" => [
                //             "district" => [
                //                 "_id" => "q666sjR_3XO",
                //                 "name" => "Asyut"
                //             ],
                //             "firstLine" => $customer_address1,
                //             "secondLine" => $customer_address2,
                //             "buildingNumber" => "123",
                //             "floor" => "4",
                //             "apartment" => "2",
                //             "isWorkAddress" => false,
                //             "zone" => [
                //                 "_id" => "CysfLBKevjl",
                //                 "name" => "Asyut"
                //             ],
                //             "city" => "Assuit"
                //         ],
                //         "returnAddress" => [
                //             "district" => "Maadi",
                //             "firstLine" => "Maadi",
                //             "secondLine" => "Nasr  City",
                //             "buildingNumber" => "123",
                //             "floor" => "4",
                //             "apartment" => "2",
                //             "isWorkAddress" => true,
                //             "zone" => "Maadi & Muqattam",
                //             "city" => "Cairo"
                //         ],
                //         "allowToOpenPackage" => true,
                //         "businessReference" => (string)$order_number,
                //         "receiver" => [
                //             "firstName" => $customer_first_name,
                //             "lastName" => $customer_last_name,
                //             "phone" => $customer_phone ?? "01113110850",
                //             "email" => $customer_email ?? "email",
                //         ]
                //     ];


                //     $response = Http::withHeaders([
                //         'Authorization' => '780388f6b8822a7aacfc57bc528347a623e59e8f7ac96f09c4203c1b372d18b8'
                //     ])
                //         ->withBody(json_encode($body), 'application/json')
                //         ->post('http://stg-app.bosta.co/api/v0/deliveries');

                //     Order::where('id', $order->id)->update([
                //         'order_id_busta' => $response['_id'],
                //         'tracking_number' => $response['trackingNumber'],
                //         'state_tracking_code' => $response['state']['code'],
                //         'state_tracking_value' => $response['state']['value'],
                //     ]);

                //     // -----------change order fulfillment status---------------

                //     // $order_data = [
                //     //     "order" => [
                //     //         "fulfillment_status" => "fulfilled",
                //     //         "note" => "test"
                //     //     ]
                //     // ];

                //     // $url = 'https://66ce4ef062e5aaa061113fdbcb1edc92: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@xn-btdbbgm6cc3bi8a1kffdahg6h.myshopify.com/admin/api/2022-01/orders/' . (string)$order_number;

                //     // $data = Http::withHeaders([
                //     //     'Accept' =>   'application/json'
                //     // ])->withBody(json_encode($order_data), 'application/json')
                //     //     ->put($url, [
                //     //         'json' => $order_data
                //     //     ]);

                //     // -----------change order fulfillment status---------------
                //     $fulfillment_data = [
                //         "fulfillment" =>
                //         [
                //             "location_id" => 59240120512,
                //             "tracking_number" => $response['trackingNumber'] . "00",
                //             "tracking_url" => "https://bosta.co/ar/tracking-shipment/?lang=ar&track_num=" . $response['trackingNumber'],
                //             "tracking_urls" => [
                //                 "https://bosta.co/ar/tracking-shipment/?lang=ar&track_num=" . $response['trackingNumber']
                //             ],
                //             "notify_customer" => true
                //         ]
                //     ];

                //     $url = 'https://66ce4ef062e5aaa061113fdbcb1edc92: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@xn-btdbbgm6cc3bi8a1kffdahg6h.myshopify.com/admin/api/2022-01/orders/' . (string)$order_number . '/fulfillments';

                //     $data = Http::withHeaders([
                //         'Accept' =>   'application/json'
                //     ])->withBody(json_encode($fulfillment_data), 'application/json')
                //         ->post($url, [
                //             'json' => $fulfillment_data
                //         ]);
                //     // return $data;
                // } catch (\Throwable $th) {
                //     return $th;
                // }
            }
        }

        return 'done';
    }

    public function sendToBosta($order_number)
    {
        $order = Order::where('order_number_shopify', $order_number)->first();
        // dd($order);
        try {
            $body = [
                "type" => 10,
                "specs" => [
                    "size" => $order->size,
                    "packageDetails" => [
                        "itemsCount" => $order->itemsCount,
                        "document" => "Document",
                        "description" => "Desc."
                    ]
                ],
                "returnSpecs" => [
                    "size" => "SMALL",
                    "packageDetails" => [
                        "itemsCount" => $order->itemsCount,
                        "document" => "Document",
                        "description" => "Desc."
                    ]
                ],
                "notes" => "Welcome Note From Fadl",
                "cod" => $order->price,
                "dropOffAddress" => [
                    "district" => $order->city,
                    "firstLine" => $order->customer_address1,
                    "secondLine" => $order->customer_address2,
                    "buildingNumber" => "",
                    "floor" => "",
                    "apartment" => "",
                    "isWorkAddress" => false,
                    "zone" => $order->city,
                    "city" => $order->customer_province
                ],
                "returnAddress" => [
                    "district" => "Maadi",
                    "firstLine" => "Maadi",
                    "secondLine" => "Nasr  City",
                    "buildingNumber" => "123",
                    "floor" => "4",
                    "apartment" => "2",
                    "isWorkAddress" => true,
                    "zone" => "Maadi & Muqattam",
                    "city" => "Cairo"
                ],
                "allowToOpenPackage" => true,
                "businessReference" => (string)$order->order_number,
                "receiver" => [
                    "firstName" => $order->customer_first_name,
                    "lastName" => $order->customer_last_name,
                    "phone" => $order->customer_phone ?? "01113110850",
                    "email" => $order->customer_email ?? "email",
                ]
            ];
            // dd($body);

            $response = Http::withHeaders([
                'Authorization' => 'b6e92ac368de74b695651468def8daf859198415242fe8e608cf32af5c092263'
            ])
                ->withBody(json_encode($body), 'application/json')
                ->post('http://app.bosta.co/api/v0/deliveries');
             // return($response);
            Order::where('id', $order->id)->update([
                'order_id_busta' => $response['_id'],
                'tracking_number' => $response['trackingNumber'],
                'state_tracking_code' => $response['state']['code'],
                'state_tracking_value' => $response['state']['value'],
                'bosta' => true,
            ]);


            $fulfillment_data = [
                "fulfillment" =>
                [
                    "location_id" => 59240120512,
                    "tracking_number" => $response['trackingNumber'],
                    "tracking_url" => "https://bosta.co/ar/tracking-shipment/?lang=ar&track_num=" . $response['trackingNumber'],
                    "tracking_urls" => [
                        "https://bosta.co/ar/tracking-shipment/?lang=ar&track_num=" . $response['trackingNumber']
                    ],
                    "tracking_company" => "bosta",
                    "notify_customer" => true
                ]
            ];

            $url = 'https://66ce4ef062e5aaa061113fdbcb1edc92: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@xn-btdbbgm6cc3bi8a1kffdahg6h.myshopify.com/admin/api/2022-01/orders/' . (string)$order->order_number_shopify . '/fulfillments';

            $data = Http::withHeaders([
                'Accept' =>   'application/json'
            ])->withBody(json_encode($fulfillment_data), 'application/json')
                ->post($url, [
                    'json' => $fulfillment_data
                ]);
            $msg = 'Done';
            return redirect()->route('tables')->withSuccess('message', 'done');
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function invoice($id)
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->get('https://66ce4ef062e5aaa061113fdbcb1edc92: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@xn-btdbbgm6cc3bi8a1kffdahg6h.myshopify.com/admin/api/2022-01/orders/' . $id);

        //  return($response->json()['order']['order_number']);
        $data['customer_first_name'] = $response->json()['order']['shipping_address']['first_name'];
        $data['customer_address1']  = $response->json()['order']['shipping_address']['address1'];
        $data['customer_phone']  = $response->json()['order']['shipping_address']['phone'];
        $data['customer_city']  = $response->json()['order']['shipping_address']['city'];
        $data['customer_zip']  = $response->json()['order']['shipping_address']['zip'];
        $data['customer_province']  = $response->json()['order']['shipping_address']['province'];
        $data['customer_country']  = $response->json()['order']['shipping_address']['country'];
        $data['customer_last_name']  = $response->json()['order']['shipping_address']['last_name'];
        $data['customer_address2']  = $response->json()['order']['shipping_address']['address2'];
        $data['customer_name'] = $response->json()['order']['shipping_address']['name'];

        $data['price'] = $response->json()['order']['current_total_price_set']['shop_money']['amount']; //360
        $data['gateway'] = $response->json()['order']['gateway']; //accept_card
        $data['order_number']  = $response->json()['order']['order_number']; //3286
        $data['created_at']  = $response->json()['order']['created_at']; //3286

        $data['items'] = $response->json()['order']['fulfillments'][0]['line_items'];
        $data['itemsCount'] = count($response->json()['order']['fulfillments'][0]['line_items']); //3 
        $data['order_id_busta'] = Order::where('order_number_shopify', $id)->first();


        if ($data['itemsCount'] < 5) {
            $size = 'SMALL';
        } elseif (in_array($data['itemsCount'], range(5, 10))) {
            $size = 'MEDIUM';
        } elseif ($data['itemsCount'] >= 10) {

            $size = 'LARGE';
        }
        $data['size'] = $size;
        // dd($data);
        return view('invoice', ['data' => $data]);
    }

    public function invoiceSave($id)
    {
        Order::where('order_number_shopify', $id)->update(['printing' => true]);
        return redirect()->route('tables');
    }
}
