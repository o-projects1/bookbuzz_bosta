<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>Orders</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-0">
                    @if (session()->has('mesmessagesage'))

                        <div wire:model="showSuccesNotification"
                            class="mt-3 alert alert-primary alert-dismissible fade show" role="alert">
                            <span class="alert-icon text-white"><i class="ni ni-like-2"></i></span>
                            <span class="alert-text text-white"> {{ session()->get('message') }}</span>
                            </button>
                        </div>
                    @endif

                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">#
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">customer
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Products</th>
                                <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    order number shopify</th>
                                <th <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    tracking number</th>
                                <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    Bosta</th>

                                <th
                                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                    Printing</th>

                                <th class="text-secondary opacity-7"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        <div class="d-flex px-2 py-1">

                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm">{{ $order->customer_first_name }}</h6>
                                                <p class="text-xs text-secondary mb-0">{{ $order->customer_phone }}
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $order->itemsCount }}</p>
                                        <p class="text-xs text-secondary mb-0">{{ $order->size }}</p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $order->order_number }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $order->tracking_number }}</p>
                                        <p class="text-xs text-secondary mb-0">{{ $order->order_id_busta }}</p>
                                    </td>
                                    {{-- <td>
                                        <p class="text-xs font-weight-bold mb-0">{{ $order->state_tracking }}</p>
                                    </td> --}}
                                    <td>
                                        @if ($order->bosta == true)
                                            <p class="text-xs font-weight-bold mb-0">Sending</p>

                                        @else
                                            <a href="{{ route('send-to-bosta', $order->order_number_shopify) }}">To
                                                Bosta</a>

                                        @endif
                                    </td>
                                    <td>
                                        @if ($order->bosta == true)
                                            @if ($order->printing == true)
                                                <a href="{{ route('invoice', $order->order_number_shopify) }}"
                                                    target="_blank">Printed</a>
                                            @else
                                                <a href="{{ route('invoice', $order->order_number_shopify) }}"
                                                    target="_blank">Invoice</a>
                                            @endif
                                        @else
                                            <p class="text-xs font-weight-bold mb-0">Send to bosta</p>

                                        @endif

                                    </td>

                                </tr>

                            @endforeach

                        </tbody>

                    </table>
                    <nav aria-label="flex-column justify-content-center">
                        {{ $orders->links() }}
                    </nav>

                </div>
            </div>
        </div>
    </div>
</div>
