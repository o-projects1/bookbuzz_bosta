<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>

    <title>Document</title>
</head>

<body>
    <div class="flex items-center justify-center min-h-screen bg-gray-100">
        <div class="w-3/5 bg-white shadow-lg">
            <div class="flex justify-between p-4">
                <div>
                    <h1 class="text-3xl italic font-extrabold tracking-widest text-indigo-500">
                        <img src="https://bosta.co/wp-content/uploads/2019/08/bosta_logo_en_red.svg" alt="bosta">
                    </h1>

                </div>
                <div class="p-2">
                    <ul class="flex">
                        <li class="flex flex-col items-center p-2 border-l-2 border-indigo-200">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-blue-600" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                            </svg>
                            <span class="text-sm">
                                www.bookbuzz.o-projects.org
                            </span>

                        </li>
                        <li class="flex flex-col p-2 border-l-2 border-indigo-200">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-blue-600" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                            </svg>
                            <span class="text-sm">
                                Size: {{ $data['size'] }}
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="w-full h-0.5 bg-indigo-500"></div>
            <div class="flex justify-between p-4">
                <div>
                    <h6 class="font-bold">Order Date : <span class="text-sm font-medium">
                            {{ date('d-m-Y', strtotime($data['created_at'])) }}</span>
                    </h6>
                    <h6 class="font-bold">Order ID : <span class="text-sm font-medium">
                            {{ $data['order_number'] }}</span></h6>
                </div>
                <div class="w-40">

                </div>
                <div class="w-40">
                    <address class="text-sm">
                        <span class="font-bold">Ship To :</span>
                        {{ $data['customer_country'] }}
                        //
                        {{ $data['customer_address1'] }}
                        //
                        {{ $data['customer_address2'] }}
                    </address>
                </div>
                <div></div>
            </div>
            <div class="flex justify-center p-4">
                <div class="border-b border-gray-200 shadow">
                    <table class="">
                        <thead class="bg-gray-50">
                            <tr>
                                <th class="px-4 py-2 text-xs text-gray-500 ">
                                    #
                                </th>
                                <th class="px-4 py-2 text-xs text-gray-500 ">
                                    Product Name
                                </th>
                                <th class="px-4 py-2 text-xs text-gray-500 ">
                                    Quantity
                                </th>
                                <th class="px-4 py-2 text-xs text-gray-500 ">
                                    Price
                                </th>

                            </tr>
                        </thead>
                        <tbody class="bg-white">

                            @foreach ($data['items'] as $key => $item)

                                <tr class="whitespace-nowrap">
                                    <td class="px-6 py-4 text-sm text-gray-500">
                                        {{ $key }}
                                    </td>
                                    <td class="px-6 py-4">
                                        <div class="text-sm text-gray-900">
                                            {{ $item['name'] ?? '' }}
                                        </div>
                                    </td>
                                    <td class="px-6 py-4">
                                        <div class="text-sm text-gray-500">{{ $item['quantity'] ?? '' }}</div>
                                    </td>
                                    <td class="px-6 py-4 text-sm text-gray-500">
                                        {{ $item['price'] ?? '' }}
                                    </td>

                                </tr>
                            @endforeach
                            {{-- <tr class="">
                                    <td colspan="3"></td>
                                    <td class="text-sm font-bold">Size</td>
                                    <td class="text-sm font-bold tracking-wider"><b>{{$data['size']}}</b></td>
                                </tr> --}}
                            <!--end tr-->
                            {{-- <tr>
                                    <th colspan="3"></th>
                                    <td class="text-sm font-bold"><b>Tax Rate</b></td>
                                    <td class="text-sm font-bold"><b>$1.50%</b></td>
                                </tr> --}}
                            <!--end tr-->
                            <tr class="text-white bg-gray-800">
                                <th colspan="3"></th>
                                <td class="text-sm font-bold"><b>Total</b></td>
                                <td class="text-sm font-bold"><b>{{ $data['price'] }}</b></td>
                            </tr>
                            <!--end tr-->

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="flex justify-between ">
                <div class="row">
                    <div class="p-4">
                        <h3 class="text-xl">BOSTA Barcode :</h3>
                        @php
                            $generator = new Picqer\Barcode\BarcodeGeneratorSVG();
                            
                        @endphp
                        {!! $generator->getBarcode($data['order_id_busta']->order_id_busta, $generator::TYPE_CODE_128) !!}

                    </div>
                    <div class="p-4">
                        <h3>Order Id</h3>
                        @php
                            $generator = new Picqer\Barcode\BarcodeGeneratorSVG();
                        @endphp
                        {!! $generator->getBarcode($data['order_number'], $generator::TYPE_CODE_128) !!}

                    </div>
                </div>
            </div>


            <div class="w-full h-0.5 bg-indigo-500"></div>
            <div class="p-4">

                <div class="flex items-end justify-end space-x-3">
                    <button class="px-4 py-2 text-sm text-green-600 bg-green-100"
                        onclick="window.print()">Print</button>
                    <a class="px-4 py-2 text-sm text-blue-600 bg-blue-100"
                        href="{{ route('invoice-save', $data['order_number']) }}">Save</a>
                </div>
            </div>

        </div>
    </div>

</body>

</html>
