<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('price')->nullable();
            $table->string('itemsCount')->nullable();
            $table->string('size')->nullable();
            $table->string('order_number_shopify')->nullable();
            $table->string('order_number')->nullable();
            $table->string('customer_first_name')->nullable();
            $table->string('customer_last_name')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('customer_email')->nullable();
            $table->text('customer_address1')->nullable();
            $table->text('customer_address2')->nullable();
            $table->string('order_id_busta')->nullable();
            $table->string('tracking_number')->nullable();
            $table->string('state_tracking_code')->nullable();
            $table->string('state_tracking_value')->nullable();
            $table->boolean('printing')->default(false);
            $table->boolean('bosta')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
