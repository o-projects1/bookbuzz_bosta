<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'price' => Str::random(10),
            'itemsCount' => Str::random(10),
            'size' => $this->faker->name(),
            'order_number_shopify' => $this->faker->name(),
            'customer_first_name' => $this->faker->name(),
            'customer_phone' => $this->faker->name(),
            'order_id_busta' => Str::random(10),
            'tracking_number' => Str::random(10),
            'state_tracking_value' => $this->faker->name(),
            'state_tracking_code' => $this->faker->name(),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
